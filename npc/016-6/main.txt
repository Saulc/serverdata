// TMW2 script
// Originals from TMW-BR
// Imported by Jesusalva

016-6,0,0,0	script	#MarineShip	NPC_HIDDEN,117,29,{
    end;

OnEvent:
    // Handle travel
    if (@timer_navio_running == 0) end;
    if      (PC_DEST$ == "Hurns") goto L_Hurns;
    else if (PC_DEST$ == "Tulim") goto L_Tulim;
    else goto L_Error;
    end;

L_Hurns:
    PC_DEST$ = "";
    LOCATION$ = "Hurns";
    @timer_navio_running = 0;
    warp "012-1", 156, 65;
    message strcharinfo(0), l("@@ disembarks at Hurnscald.", strcharinfo(0));
    goto L_CleanUp;

L_Tulim:
    PC_DEST$ = "";
    LOCATION$ = "Tulim";
    @timer_navio_running = 0;
    warp "003-1", 119, 25;
    message strcharinfo(0), l("@@ disembarks at Tulimshar.", strcharinfo(0));
    goto L_CleanUp;

L_Error:
    debugmes "ERROR: PLAYER INVALID PC_DEST ON #MarineShip: " + PC_DEST$;
    PC_DEST$ = "";
    @timer_navio_running = 0;
    warp "000-1", 22, 22;
    dispbottom l("An error on your travel happened. Please report.");
    goto L_CleanUp;

L_CleanUp:
    // Clean up pirate ship, if needed.
    if (mobcount("016-7", "#MarineShipAttack::OnPirateDie") && !getmapusers("016-7")) {
        killmonster("016-7", "All");
    }
    if (mobcount("016-7", "#MarineShipAttack::OnVictory") && !getmapusers("016-7")) {
        killmonster("016-7", "All");
    }
    if (mobcount("016-7", "#MarineShipAttack::OnVictory") > 1) {
        killmonster("016-7", "#MarineShipAttack::OnVictory");
        monster "016-7", 38, 27, "Pirate Captain", OceanPirate, "#MarineShipAttack::OnVictory";
    }
    end;
}

