// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Special Soul Menhir which only allows leaving the map.

000-1,22,22,0	script	Emergency Exit	NPC_SOUL_CURSED,2,2,{
OnTouch:
OnTalk:
OnTalkNearby:
    if (getsavepoint(0) != "000-1") warp getsavepoint(0), getsavepoint(1), getsavepoint(2);
    if (getsavepoint(0) != "000-1") end;
    savepoint "002-1", 53, 38;
    warp "002-1", 53, 38;
    end;

}

