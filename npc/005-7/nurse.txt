// TMW-2 Script
// Author:
//    Jesusalva
//    Crazyfefe

005-7,41,30,0	script	Candor's Nurse	NPC_FEMALE,{

    //if (sc_check(SC_POISON)) goto L_CurePoison;
    if (Hp < (MaxHp/100*90)) goto L_Heal;

    mesn;
    mesq lg("Hello, dear! I love @@, could you bring me some? They're sooooo sweet and delicious!","Hello, dear! I love @@, could you bring me some? They're sooooo sweet and delicious!", getitemlink(Candy) );
    mesq l("Ah, I can also cure you, if you need.");

    do
        {
        select
            rif(Hp < MaxHp, l("Please heal me!")),
            rif(countitem(Candy) > 0, l("Here, I have a candy!")),
            l("I'm fine, no worries...");

        switch (@menu)
        {
            case 1:
                goto L_Heal;
                break;
            case 2:
                goto L_Help;
                break;
            case 3:
                goto L_Close;
                break;
        }
        } while (@menu != 3);

L_Help:
    mes "";
    mesn;

    if (countitem(Candy) == 0)
    {
        mesq l("You don't seem to have any @@ with you!", getitemlink(.Item));
        close;
    }
    delitem .Item, 1;
    mesq l("Thanks a lot!");
    if (getq(CandorQuest_Nurse) == 0)
    {
        getexp 21,0;
        Zeny = Zeny + 200;
        setq CandorQuest_Nurse, 1;
        close;
    }
    if (getq(CandorQuest_Nurse) == 1)
    {
        getexp 11,0;
        Zeny = Zeny + 100;
        setq CandorQuest_Nurse, 2;
        close;
    }
    if (getq(CandorQuest_Nurse) == 2)
    {
        getexp 6,0;
        Zeny = Zeny + 50;
        setq CandorQuest_Nurse, 3;
        close;
    }
    getexp 5,0;
    Zeny = Zeny + 25;
    close;

L_Heal:
    Nurse(.name$, 10, 5);
    close;

L_Close:
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonShorts);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ShortTankTop);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 14);

    // quest setup
    .Item = Candy;

    .sex = G_FEMALE;
    .distance = 5;
    end;

}

