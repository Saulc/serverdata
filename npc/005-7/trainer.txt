// TMW2 scripts.
// Authors:
//    Jesusalva
// Originals:        Ernando <ernando.quirino@hotmail.com>
// Original review:  Lunovox <rui.gravata@gmail.com>; Ernando <ernando.quirino@hotmail.com>; Jesusalva <supremo@brasil.byethost22.com>
// Objective:        Train the player and give some experience.
// License:          GPL v3
//
// CandorQuest_Trainer
//  0:  Not started

//  1:  Took Maggot Quest
//  2:  Completed Maggot Quest
//  3:  Awaiting next quest

//  4:  Took House Maggot Quest
//  5:  Completed House Maggot Quest
//  6:  Awaiting next quest

//  7:  Took Candor Scorpion Quest
//  8:  Completed Candor Scorpion Quest
//  9:  Awaiting next quest

// 10:  Took Scorpion Quest
// 11:  Completed Scorpion Quest
// 12:  Finished all trainment

005-7,25,29,0	script	Trainer	NPC_PLAYER,{
    .@q=getq(CandorQuest_Trainer);
    .@b=getq(ShipQuests_Knife); // TODO: We should check if user is with weapon equipped instead
    mesn;
    mesq lg("Hello my friend! Need my help?");
    next;
    goto L_Menu;

L_PreMenu:
    mesn;
    mesq l("Do you need my help?");
    next;
    goto L_Menu;

L_Menu:
    mesn strcharinfo(0);
    menu
        l("How can you help me?"),              L_ExplicaSiProprio,
        rif(getq(CandorQuest_Trainer) < 15, l("I want to be trained!")), L_Trainment,
        l("I wanted info about how to play."),  L_Manaplus_gap,
        l("How do I make money?"),              L_ExplicaGrama,
        l("Monsters."),                         L_Monstros,
        l("Quests."),                           L_Quests,
        l("NPCs."),                             L_NPC,
        l("Magic."),                            L_Magica,
        l("Rules."),                            L_Regras,
        l("No, thanks!"),                       L_Fechar;


L_Manaplus_gap:
    mes "";
    goto L_Manaplus;

L_Manaplus:
    mesn;
    mesq l("What do you want to learn more about?");
    next;
    mesn strcharinfo(0);
    menu
        l("How I see my items?"),               L_ExplicaBagagem,
        l("How trade with other players?"),     L_ExplicaTrade,
        l("How hunt monsters?"),                L_ExplicaBatalha,
        l("How talk with someone?"),            L_ExplicaDialogo,
        l("Status."),                           L_Status,
        l("Styles."),                           L_Estilo,
        l("Commands."),                         L_Comandos,
        l("Shortcuts."),                        L_Teclado,
        l("I changed my mind."),                L_Menu_gap;

L_Trainment:
    .@q=getq(CandorQuest_Trainer);
    mes "";
    if (BaseLevel < 3) goto L_NoLevel;
    if (.@b == 0) goto L_NoKnife;
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)

    mesn;
    if (.@q == 0) {
        mesq l("Ok, you have some level. Now, let me think on an easy task for you...");
        next;
        mesq l("Ah! I know. Kill 10 @@. They usually are the crops.", getmonsterlink(Maggot));
        setq CandorQuest_Trainer, 1, 0;
    } else if (.@q == 1) {
        mesq l("You are killing @@. They are usually at the crops.", getmonsterlink(Maggot));
    } else if (.@q == 2) {
        mesq l("Good job! Here is your reward! (40 xp 25 gp)");
        getexp 30, 0;
        set Zeny, Zeny + 25;
        setq CandorQuest_Trainer, 3, 0;
    } else if (.@q == 3) {
        mesq l("Less maggots means more food to the town. So, this time the monsters will be a little stronger.");
        next;
        mesq l("In the storehouse, kill 5 @@. They keep respawning and are annoying.", getmonsterlink(HouseMaggot));
        setq CandorQuest_Trainer, 4, 0;
    } else if (.@q == 4) {
        mesq l("You are killing @@ at the storehouse.", getmonsterlink(HouseMaggot));
    } else if (.@q == 5) {
        mesq l("Perfect! Here is your reward! (40 xp 25 gp)");
        getexp 42, 0;
        set Zeny, Zeny + 25;
        setq CandorQuest_Trainer, 6, 0;
    } else if (.@q == 6) {
        mesq l("As you see, helping others is a good way to level up. You can also sell monster parts for some money.");
        next;
        mesq l("So, please kill 3 @@, which are usually at beaches. You can sell their stingers.", getmonsterlink(CandorScorpion));
        setq CandorQuest_Trainer, 7, 0;
    } else if (.@q == 7) {
        mesq l("You are killing @@ at the beach.", getmonsterlink(CandorScorpion));
    } else if (.@q == 8) {
        mesq l("Managed to drop anything? It requires more luck than skill. Here is your reward. (40 xp 25 gp)");
        getexp 53, 0;
        set Zeny, Zeny + 25;
        setq CandorQuest_Trainer, 9, 0;
    } else if (.@q == 9) {
        mesq l("This will be the last task. Besides the @@, the strongest monster on this island worth killing is the @@.", getmonsterlink(ManaBug), getmonsterlink(Scorpion));
        next;
        mesq l("I'll ask you to kill 2 @@. This will prove your worth. They are northwest of the island.", getmonsterlink(Scorpion));
        setq CandorQuest_Trainer, 10, 0;
    } else if (.@q == 10) {
        mesq l("You are killing @@ at northwest from the island.", getmonsterlink(Scorpion));
    } else if (.@q == 11) {
        mesq l("Congratulations! Here is your reward (40 xp 25 gp).");
        mesq l("You completed your trainment, so you're getting 50 bonus experience points. If you level up, use your stat points wisely!");
        getexp 70, 0;
        set Zeny, Zeny + 25;
        setq CandorQuest_Trainer, 12, 0;
    } else if (.@q == 12 && BaseLevel >= 9) {
        mesq l("You are already fully trained. You should go to Tulimshar, but if you want a bonus task, please kill a @@.", getmonsterlink(ManaBug));
        setq CandorQuest_Trainer, 13, 0;
    } else if (.@q == 12 && BaseLevel < 9) {
        mesq l("You are already fully trained. You should go to Tulimshar. I may have another task for you later, but you are too weak now, get some levels.");
    } else if (.@q == 13) {
        mesq l("You are trying to kill a @@.", getmonsterlink(ManaBug));
    } else if (.@q == 14) {
        mesq l("Wow! You did it! I do not think anyone else could have done that.");
        mesq l("Here, take this @@ - you deserve it! And here is 200 GP to buy a better weapon.", getitemlink(CandorHeadBand));
        inventoryplace CandorHeadBand, 1;
        getexp 80, 5;
        getitem CandorHeadBand, 1;
        set Zeny, Zeny + 200;
        setq CandorQuest_Trainer, 15, 0;
    }
    next;
    goto L_PreMenu;

L_NoLevel:
    mesn;
    mesq l("Ah, yes... You see, there is just no task I can give to you right now. ##BYou are too weak to fight monsters.##b");
    next;
    mesn;
    mesq l("Try doing quests which doesn't involve monster hunting first. I'm sure ##B Ayasha ##b could use your help.");
    next;
    mesn;
    mesq l("Otherwise, the monsters here usually won't attack you unless provoked.");
    next;
    mesn;
    mesq l("Complete quests, gain some experience, allocate some status, and you'll be ready for my training.");
    next;
    goto L_PreMenu;

L_NoKnife:
    mesn;
    mesq l("And with what you expect to fight monsters? You must arrange yourself a weapon!");
    next;
    mesn;
    mesq l("I don't know. Try getting a Rusty Knife or something. Maybe the chef of Nard's ship can spare you one.");
    next;
    mesn;
    mesq l("There probably is a huge, flashing orange exclamation mark over a suitable knife you could take and nobody would mind.");
    next;
    goto L_PreMenu;

// Anything below this line is copy-pasted from SG:ManaBird, a TMW-BR clone
// Do not attention to it, unless you need to. Translated by Google.
L_ExplicaSiProprio:
    mes "";
    mesn;
    mesq l("Saxso, the former mayor, commanded me to strengthen the youngsters, so that they have sufficient power to fight monsters.");
    next;
    mesn;
    mesq lg("He died, but I plan in fulfilling his will. I can give you training for that, and teach you how to fight properly.");
    next;
    mesn;
    mesq l("I see you have arms long enough to be an archer.");
    next;
    goto L_PreMenu;

L_ExplicaGrama:
    mes "";
    mesn;
    mesq l("Merchants like to buy body parts of killed monsters and animals because they can make items and equipment.");
    next;
    mesn;
    mesq l("Some others also like to buy them to keep as trophies. Either way, you can make some money with that.");
    next;
    mesn;
    mesq l("You can also make money ##Bdoing quests##b. Elmo will tell you almost every quest which can be done in Candor.");
    next;
    goto L_PreMenu;

L_ExplicaBagagem:
    mes "";
    mesn;
    mesq l("You can see all your equipment by pressing the F3 key.");
    next;
    mesn;
    mesq l("To equip or unequip an item, select it and press the 'Equip' or 'Unequip' button. You can not 'Equip' or 'Unequip' when talking to someone.");
    next;
    mesn;
    mesq l("Dress up! Do not walk without clothes! Always wear your items! They leave you less vulnerable to attacks and stronger to defeat your opponents.");
    next;
    mesn;
    mesq l("To discard an item you no longer want, select it and press the 'Discard' button. Generic items can be discarded or sold. But equipment can only be sold.");
    next;
    mesn;
    mesq l("There are three types of items.");
    mesq l("Those for consumption, equipment and generics.");
    next;
    mesn;
    mesq l("Items for consumption, like potions, can only be used once.");
    mesq l("Once used, they will disappear from your inventory.");
    next;
    mesn;
    mesq l("Equippable items are armour, weapons and accessories.");
    mesq l("They can be equipped to make your look more interesting or to improve some of its features.");
    next;
    mesn;
    mesq l("Generic items are used for different purposes. In creating other items, to swap and sell, to collect, etc.");
    next;
    goto L_Manaplus;

L_ExplicaTrade:
    mes "";
    mesn;
    mesq l("Press the 'R' key to ignore or accept business proposals. You and the other citizen who want to negotiate need to be in the configuration that accepts negotiations. if your configuration is 'Ignoring business proposals', then you will not receive the warning from any citizen wanting to negotiate with you, and you will not be able to initiate negotiations.");
    next;
    mesn;
    mesq l("To negotiate with other citizens, you should click the second mouse button on some other citizen who is accepting negotiations, and select the 'Negotiation' option from the menu that will appear.");
    next;
    mesn;
    mesq l("After you have confirmed the negotiation, a window with a vertical split will appear. The left side are the items you intend to offer in trading. The right side are the items that the other citizen intends to offer in trading.");
    next;
    mesn;
    mesq l("Open your inventory window (F3 key) next to the trading window. Select an item you want to offer, and then press the Add button. To add money to the negotiation, enter the amount you will offer and press the Change button.");
    next;
    mesn;
    mesq l("When you have added all the items and money you want, press the 'Propose Business' button. The other citizen must also press the 'Propose Business' button.");
    next;
    mesn;
    mesq l("if the proposal is not convenient for you, just close the trading window to cancel the exchange of items and money. But if both press the 'Accept Negotiation' button, then the marketing will be finished.");
    next;
    mesn;
    mesq l("Remember! You're trading things, not lending/borrowing them. You are solely responsible for everything you own.");
    next;
    goto L_Manaplus;

// TODO: We have over nine instructions here. You usually can only memorise from three to five at a time!
L_ExplicaBatalha:
    mes "";
    mesn;
    mesq l("Note down. To hunt a target you must click the primary mouse button on it. Avoid fighting monsters or citizens much stronger than you. ##BYou will lose experience if you are defeated.##b");
    next;
    mesn;
    mesq l("Within the cities is a place safe enough not to be attacked by another person (except during wars). But outside of them there are some places where the citizen can be attacked by enemies from other realms, or even by someone from the same realm.");
    next;
    mesn;
mesq l("There are some stones scattered around the world that mark your point of return in case of defeats. Some ship chests may also serve as a return point. You can also select some beds in case of defeats.");
    next;
    mesn;
    mesq l("Almost all creatures drop useful items when defeated. To get the dropped item press the 'Z' key next to the item or click the primary button on the item.");
    next;
    mesn;
    mesq l("To focus on a creature, press the 'A' key. To focus on another citizen, press the 'Q' key. To attack the focused target press the 'X' key or click the primary button on the creature.");
    next;
    mesn;
    mesq l("To focus on an NPC, press the 'N' key. To talk to him press the 'T' key.");
    next;
    mesn;
    mesq l("To defocus or stop attacking, press Shift + A.");
    next;
    mesn;
    mesq l("You can, however, use ##BCtrl##b to auto-select a monster and attack them. This usually also collects drops, but press Z to be sure.");
    next;
    goto L_Manaplus;

L_ExplicaDialogo:
    mes "";
    mesn;
    mesq l("To display the dialog box with other citizens, press the F7 key.");
    next;
    mesn;
    mesq l("To speak in public select the 'General' tab. It serves to talk to people who are appearing on your screen.");
    next;
    mesn;
    mesq l("To speak privately with someone, click the second mouse button on the citizen and select the 'Whisper' option.");
    next;
    mesn;
    mesq l("In order to enter a message press the 'Enter' key, this will display the white box of typing. Type your message there and press 'Enter' again to send your speech.");
    next;
    mesn;
    mesq l("To speak privately to a friend who is not appearing on your screen, type the command '##B /q Citizen Name ##b' and press 'Enter'. This command will open a long-distance dialog that has the name of who you want to talk to. Select this new tab and send your message through it.");
    next;
    mesn;
    mesq l("But be careful: do not scream when using a lot of capital letters, and do not keep repeating the lines, or you may be severely penalized.");
    next;
    goto L_Manaplus;

L_Fechar:
    close;

L_Monstros:
    mes "";
    mesn;
    mesq l("Monsters are everywhere. They're a plague we're trying to get rid of.");
    next;
    mesn;
    mesq l("There are three types of monsters: the aggressive, the neutral, and the collaborative.");
    next;
    mesn;
    mesq l("Aggressors always know when they are in danger! Therefore, they are always on standby, attacking anyone who appears ahead.");
    next;
    mesn;
    mesq l("Neutral monsters do not have such a sense of danger.");
    mesq l("They will not attack anyone unless they are attacked first.");
    next;
    mesn;
    mesq l("Normally, collaborative behave like neutral monsters. Unless some partner of the same species is in danger, at which point they all take an aggressive stance against the aggressor.");
    mesq l("It's always good to see if you have a lot of them around before you think about attacking one!");
    next;
    goto L_PreMenu;

L_Estilo:
    mes "";
    mesn;
    mesq l("NPC stylists will cut your hair!");
    mesq l("They are known to use a revolutionary hair growth formula.");
    next;
    goto L_Manaplus;

L_Quests:
    mes "";
    mesn;
    mesq l("There are people in the world who need help!");
    mesq l("Most of these people will not think twice before giving a nice reward to anyone who helps them.");
    mesq l("So be nice and help people along the way!");
    next;
    mesn;
    mesq l("Seldomly, they'll have an exclamation mark over their heads. But some quests are hidden, so talk to people and have fun!");
    next;
    goto L_PreMenu;

L_NPC:
    mes "";
    mesn;
    mesq l("NPCs(Non Playable Characters) or non-playable characters are characters that are always in the game, offering a wide variety of reactions, from a simple friendly conversation to a desperate request for help.");
    next;
    mesq l("##BIMPORTANT:##b People usually doesn't shout, they talk. Because this, if you are too far, an NPC won't hear you.");
    mesq l("When this is the case, you should get closer to the NPC, until they hear you.");
    mesq l("If you are above the NPC and they still doesn't hear you, this mean they are deaf - you should report this!");
    goto L_PreMenu;

L_Comandos:
    mes "";
    mesn;
    mesq l("/ clear clears the text box.");
    mesq l("/ whisper [name] allows you to send a private message to the player. if [name] contains spaces, it must be enclosed in quotation marks.");
    //mesq l("/who mostra o número de jogadores conectados no momento.");
    mesq l("/ present shows the number of people in the neighbourhood.");
    mesq l("/ where shows the name of the map you are in.");
    mesq l("/ help explains how to use all client commands.");
    mesq l("@commands lists even more advanced commands, but you can't use all of them.");
    next;
    goto L_Manaplus;

L_Status:
    mes "";
    mesn;
    mesq l("People vary greatly in the amount of strength, agility, dexterity, intelligence, vitality and luck.");
    next;
    mesn;
    mesq l("Strength helps you carry more items and also gives you a more forceful blow, but ends up not being very interesting if you focus on weapons that use projectiles, such as the bow.");
    mesq l("Greater agility allows you to attack faster and has a greater chance of evading attacks.");
    mesq l("Your dexterity determines your ability to hit monsters and is valuable to players who prefer weapons that use projectiles.");
    next;
    mesn;
    mesq l("Vitality determines how resistant you are to attacks and how many blows you can take before you die. It also affects status effects, like poison.");
    mesq l("Intelligence is very useful for alchemy and magic, but nowadays there are few opportunities to use it.");
    mesq l("Your luck determines several small things, including the number of critical attacks you are going to suffer and perform.");
    next;
    mesn;
    mesq l("I recommend that you train your dexterity a great deal, since most monsters out there are hard to hit without it.");
    mesq l("For now do not take too much time to work on your intelligence, after all, almost nobody have magic this day.");
    next;
    goto L_Manaplus;

L_Magica:
    mes "";
    mesn;
    mesq l("Magic is dead. Well, not yet, we still have some mana stones left - but only the strongest ones are allowed to use them and acquire magic.");
    next;
    goto L_PreMenu;

L_Teclado:
    mes "";
    mesn;
    mesq l("There are many key combinations, press F1 to see a short list of them!");
    next;
    goto L_Manaplus;

L_Regras:
    mes "";
    callfunc "GameRules";
    next;
    goto L_PreMenu;

L_Menu_gap:
    mes "";
    goto L_PreMenu;

    function trainer_add_kills
    {
        .@qp=getq(CandorQuest_Trainer);
        .@kp=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
        setq CandorQuest_Trainer, .@qp, .@kp+1;
        //message strcharinfo(0), l("Set status @@ with @@ kills", .@qp, .@kp);
    }

    function trainer_max_kills
    {
        .@qp=getq(CandorQuest_Trainer);
        setq CandorQuest_Trainer, .@qp+1, 0;
        //message strcharinfo(0), l("End status @@", .@qp);
    }

OnKillMaggot:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 1) {
        if (.@k+1 >= 10) {
            trainer_max_kills();
            message strcharinfo(0), l("All maggots are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/10 Maggots", .@k+1);
        }
    }
    end;
OnKillHouseMaggot:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 4) {
        if (.@k+1 >= 5) {
            trainer_max_kills();
            message strcharinfo(0), l("All house maggots are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/5 House Maggots", .@k+1);
        }
    } else {
        if (is_staff()) 
            dispbottom "It's working. (T:OKHM)";
    }
    end;
OnKillCandorScorpion:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 7) {
        if (.@k+1 >= 3) {
            trainer_max_kills();
            message strcharinfo(0), l("All candor scorpions are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/3 Candor Scorpions", .@k+1);
        }
    }
    end;
OnKillScorpion:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 10) {
        if (.@k+1 >= 2) {
            trainer_max_kills();
            message strcharinfo(0), l("All scorpions are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/2 Scorpion", .@k+1);
        }
    }
    end;
OnKillManaBug:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 13) {
        if (.@k+1 >= 1) {
            trainer_max_kills();
            message strcharinfo(0), l("All mana bugs are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/1 Mana Bug", .@k+1);
        }
    }
    end;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CottonTrousers);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 20);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .sex = G_MALE;
    .distance = 5;
    end;
}
