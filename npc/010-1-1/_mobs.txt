// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 010-1-1: Canyon Great Caves mobs
010-1-1,55,58,29,22	monster	Cave Snake	1035,8,35000,150000
010-1-1,70,40,3,1	monster	Small Topaz Bif	1101,1,35000,150000
010-1-1,79,54,59,54	monster	Cave Maggot	1027,55,40000,200000
010-1-1,24,34,4,4	monster	Black Scorpion	1071,2,35000,150000
010-1-1,112,34,4,4	monster	Black Scorpion	1071,2,35000,150000
010-1-1,147,83,29,22	monster	Black Scorpion	1074,12,35000,150000
010-1-1,124,43,29,22	monster	Bat	1039,3,35000,150000
010-1-1,135,63,54,54	monster	Snake	1122,20,40000,200000
010-1-1,92,90,54,48	monster	Small Topaz Bif	1101,4,40000,200000
