// TMW2 Script
// Author:
//      SkyDragon
//      Saulc
// Reviewer:
//    Jesusalva
// Description:
//    Caelum Miner, main author Skydragon Caelum mean Sky in Latin.
// id:193  MineQuests_Caelum

007-1,143,49,0	script	Caelum	NPC_PLAYER,{
    .@Miner = getq(MineQuest_Caelum);
    if (BaseLevel < 28) goto L_TooWeak;
    if (.@Miner == 1) goto L_Check;
    if (.@Miner == 2) goto L_Complete;

L_GiveTask:
    mesn;
    mesq lg("Hello, wanderer!");
    next;
    mesq l("How did you end up here?");
    next;

    menu
        l("To be honest, I don't know. I was only walking, seeking for adventure!"), L_Quest,
        l("It's none of your business."), L_Quit;


L_Quest:
    mes "";
    mesn;
    mesq l("You perchance said in adventure? Good, because I just hurt my arm while mining!");
    next;
    mesn;
    mesq l("If you bring me some items to I do a bandage and heal myself, I'll give you my gloves!");
    next;

    menu
        l("Really? What do you need?"), L_Start,
        l("Better do this some other time..."), L_Quit;


L_Start:
    setq MineQuest_Caelum, 1;
    mes "";
    mesn;
    mesq l("Ok, what I need is:");
    goto L_List;

L_Quit:
    mes "";
    mesn;
    mesq l("Alright.");
    close;

L_List:
    mes "";
    mesn;
    mes l("Here's what I need:");
    mes l("@@/1 @@", countitem(Lifestone), getitemlink(Lifestone));
    mes l("@@/1 @@", countitem(CottonCloth), getitemlink(CottonCloth));
    mes l("@@/2 @@", countitem(BlackScorpionStinger), getitemlink(BlackScorpionStinger));
    mes l("@@/5 @@", countitem(ScorpionClaw), getitemlink(ScorpionClaw));
    mes l("@@/10 @@", countitem(BatTeeth), getitemlink(BatTeeth));
    mes l("@@/10 @@", countitem(CactusDrink), getitemlink(CactusDrink));
    mes l("@@/20 @@", countitem(MaggotSlime), getitemlink(MaggotSlime));
    mes l("@@/8 @@, just because I'm hungry as a bear.", countitem(BugLeg), getitemlink(BugLeg));
    close;

L_Check:
    mesn;
    mesq l("Did you brought me everything I asked for?");
    next;
    menu
        l("Yes!"), L_Give,
        l("I forgot what you need!"), L_List,
        l("No!"), L_Quit;

L_Give:
    if (
        countitem(Lifestone)            < 1  ||
        countitem(CottonCloth)          < 1  ||
        countitem(BlackScorpionStinger) < 2  ||
        countitem(BugLeg)               < 8  ||
        countitem(BatTeeth)             < 10 ||
        countitem(CactusDrink)          < 10 ||
        countitem(MaggotSlime)          < 20 ||
        countitem(ScorpionClaw)         < 5
        ) goto L_Lying;

    inventoryplace MinerGloves, 1;

    delitem(Lifestone, 1);
    delitem(CottonCloth, 1);
    delitem(BlackScorpionStinger, 2);
    delitem(BugLeg, 8);
    delitem(BatTeeth, 10);
    delitem(CactusDrink, 10);
    delitem(MaggotSlime, 20);
    delitem(ScorpionClaw, 5);

    getitem(MinerGloves, 1);
    getexp(855, 3);
    setq(MineQuest_Caelum, 2);

    mes "";
    mesn;
    mesq l("Here, all yours. I can't use them like I am now. Thank you.");
    close;

L_Complete:
    mesn;
    mesq l("Wandering too much? Take care to don't get lost.");
    close;

// Funnier to write than to read, but the player lied. :angel:
L_Lying:
    mesn;
    mesq l("No no no, that's wrong.");
    next;
    mesc l("The miner goes to count your stuff again.");
    next;
    mesc l("And again.");
    next;
    mesc l("And again, and again.");
    next;
    mesc l("And again, and again, again.");
    next;
    mesc l("You wonder, maybe he entered on an infinite loop? Hellooo, anybody home?");
    next;
    mesn;
    mesq l("No no no, you don't have everything I've asked for!");
    next;
    goto L_List;

L_TooWeak:
    mesn;
    mesq l("Wanderer, here is dangerous! Go back!");
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, MinerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 4);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 3);

    .sex = G_MALE;
    .distance = 4;

    end;
}
