// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Old pirate with quests.
// LOFQuest_George
//  1 - Roger assigned the task
//  2 - First task accepted
//  3 - First task complete
//  4 - Second task accepted
//  5 - Second task complete

018-1,26,36,0	script	George	NPC_GEORGE,{
    .@q=getequipid(EQI_HEAD_TOP);
    .@q1=getequipid(EQI_ACC_R);
    if (.@q == CorsairHat) goto L_Pirate;
    if (.@q == PirateBandana) goto L_Bandana;
    if (.@q == RightEyePatch || .@q1 == EyePatch) goto L_EyePatch;

    mesn l("George the Pirate");
    mesq l("Yar! Do you need something, matey?");
    goto L_Main;

// Intro dialogs
L_Pirate:
    mesn l("George the Pirate");
    mesq l("Arrrrh! Ye be wearing a pirate's hat!");
    goto L_Main;

L_Bandana:
    mesn l("George the Pirate");
    mesq l("Arrrrh! Ye remind me of my old first mate!");
    goto L_Main;

L_EyePatch:
    mesn l("George the Pirate");
    mesq l("Arrrrh! Looks like ye lost an eye there!");
    goto L_Main;

// Main Menu
L_Main:
    .@q=getq(LoFQuest_George);

    menu
        l("What is this island?"), L_Explain,
        rif(.@q == 1, l("Actually, I heard from Roger that you may need some help.")), L_RequestOne,
        rif(.@q == 2, l("About the items you asked me to collect...")), L_ProgressOne,
        rif(.@q == 3 && 0, l("Do you need any more help?")), L_RequestTwo,
        rif(.@q == 4 && 0, l("About the items you asked me to collect...")), L_ProgressTwo,
        l("Nothing I guess"), L_Close;

// Context menu
L_Explain:
    mesn l("George the Pirate");
    mesq l("Arrr matey! This is @@, yarr.", l("##BSincerity Island##b"));
    next;
    mesn l("George the Pirate");
    mesq l("Nobody lives here for years, arr. Monsters have taken over everything, yarr.");
    next;
    mesn l("George the Pirate");
    mesq l("It is a pirate treasure hideout, yarr arr! If you have a @@ and a @@, you may get luck, yarr!", getitemlink(PirateTreasureMap), getitemlink(IronShovel));
    next;
    goto L_Main;


// Requests
L_RequestOne:
    if (BaseLevel < 40)
        goto L_Weak;
    mesn l("George the Pirate");
    mesq l("Roger, eh? Aye, I do need help. If you bring me 30 @@, 50 @@, and 30 @@, maybe I can give you a reward.", getitemlink(Beer), getitemlink(EmptyBottle), getitemlink(CasinoCoins));
    setq LoFQuest_George, 2;
    close;

L_RequestTwo:
    if (BaseLevel < 60)
        goto L_Weak;
    mesn l("George the Pirate");
    mes "\"Arrr matey! My captain asked me to have you get him some items. Please bring us 50 zombie nachos and 50 lady fingers, my captain likes to snack on those during our long voyages. Oh, and something to read too!\"";
    setq LoFQuest_George, 4;
    close;

// Progress Report 1
L_ProgressOne:
    if (countitem(Beer) < 30 ||
        countitem(EmptyBottle) < 50 ||
        countitem(CasinoCoins) < 30)
        goto L_NotEnough;

    mesn l("George the Pirate");
    mesq l("Did you brought everything I ask for?");
    select
        l("Yes"),
        l("Not sure");

    if (@menu == 2)
        goto L_NotEnough;

    mesn l("George the Pirate");
    mesq l("Yar matey! You have the items I asked for! You have the honor and trust we pirates bestow each other!");

    inventoryplace SailorHat, 1;
    delitem Beer, 30;
    delitem EmptyBottle, 50;
    delitem CasinoCoins, 30;
    mes "";
    mesq l("Here is a reward for your effort! Now you can be a true buccaneer!");
    getitem SailorHat, 1;
    getexp 5000, 0;
    setq LoFQuest_George, 3;
    goto L_Close;

// Progress Report 2
L_ProgressTwo:
    if (countitem(WarlordPlate) < 100 ||
        countitem(WarlordPlate) < 50 ||
        countitem(WarlordPlate) < 1)
        goto L_NotEnough;

    mesn l("George the Pirate");
    mesq l("Did you brought everything I ask for?");
    select
        l("Yes"),
        l("Not sure");

    if (@menu == 2)
        goto L_NotEnough;

    mesn l("George the Pirate");
    mes "\"Arrr! Good job matey! You brought us everything we asked for, even the Maritime Almanac Scroll!\"";

    inventoryplace CorsairHat, 1;
    delitem WarlordPlate, 50;
    delitem WarlordPlate, 50;
    delitem WarlordPlate, 1;
    mes "\"My captain wanted me to give this to you as a special reward for your efforts.\"";
    getitem CorsairHat, 1;
    getexp 15000, 0;
    setq LoFQuest_George, 5;
    goto L_Close;

// Fallbacks
L_Weak:
    mesn l("George the Pirate");
    mesq l("Arr, you are not experienced enough to help me yet!");
    close;

L_NotEnough:
    mesn l("George the Pirate");
    mesq l("Arrr! You did not bring me everything I asked for!");
    if (.@q == 2)
        goto L_NotEnoughOne;
    if (.@q == 4)
        goto L_NotEnoughTwo;
    close;

L_NotEnoughOne:
    mesq l("Remember, I need 30 @@, 50 @@, and 30 @@.", getitemlink(Beer), getitemlink(EmptyBottle), getitemlink(CasinoCoins));
    goto L_Close;

L_NotEnoughTwo:
    mesq l("Remember, I need 100 @@, 50 @@, and something interesting to read.", getitemlink(WarlordPlate), getitemlink(CorsairHat));
    goto L_Close;

L_Close:
    sailortalk;
    closedialog;
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

