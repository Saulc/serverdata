// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Leader of the BERSERK class

003-0,34,45,4	script	Ragger Master	NPC_PLAYER,{
    if (!(MAGIC_SUBCLASS & CL_BERSERKER))
        goto L_SignUp;
    goto L_Member;

// Sign Up
L_SignUp:
    // Not allowed if subclass filled or not from main class
    if (total_subclass() >= max_subclass() || getskilllv(MAGIC_WARRIOR) < 2)
        goto L_Close;
    mesn;
    mesq l("Hey there! Do you want to join the Berserk (Ragger) Class?");
    mesc l("Warning: If you join a subclass, you can't leave it later!"), 1;
    next;
    if (askyesno() != ASK_YES)
        close;
    // TODO: Requeriment for signing up to a subclass? Or is the tier + skill quest hard enough?
    MAGIC_SUBCLASS=MAGIC_SUBCLASS|CL_BERSERKER;
    mesn;
    mesq l("Welcome to the berserker guild!");
    close;

// Close
L_Close:
    goodbye;
    closedialog;
    close;

L_Missing:
    mesn;
    mesq l("Hey hey! You don't have that stuff, CAN'T YOU READ?!");
    percentheal 0, -10;
    next;
    goto L_Member;

// Membership area
// Ragger
// KN_AUTOCOUNTER (Counter any attack with 2x critical rate and block attack for 0.5s)
// SM_ENDURE (deny stun penalty when hit, and MDEF bonus, for 10s)
// KN_TWOHANDQUICKEN (raise attack speed in 30%, if you are with a 2-hands sword) (30s/level)

L_Member:
    mesn;
    mesq l("Hey there! Do you want to learn new skills for a very small teaching fee?");
    select
        rif(sk_intcost(SM_BASH) && getskilllv(SM_BASH) < (3+degree_subclass()), l("Improve Bash Skill")),
        rif(sk_intcost(SM_ENDURE) && sk_canlvup(SM_ENDURE), l("Improve Endure Skill")),
        rif(sk_intcost(KN_AUTOCOUNTER) && sk_canlvup(KN_AUTOCOUNTER), l("Improve Counter Defense")),
        rif(sk_intcost(KN_TWOHANDQUICKEN) && sk_canlvup(KN_TWOHANDQUICKEN), l("Improve Two Hands Quicken")),
        l("Leave Subclass"),
        l("Nothing at the moment.");
    mes "";
    switch (@menu) {
        case 1:
            mesc l("[Bash]");
            mesc l("Blow with increased attack and precision.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(ManaPiouFeathers),  (getskilllv(SM_BASH)+1)*15,  getitemlink(ManaPiouFeathers));
            mesc l("@@/@@ @@", countitem(CaveSnakeSkin),     (getskilllv(SM_BASH)+1)*4,  getitemlink(CaveSnakeSkin));
            mesc l("@@/@@ @@", countitem(RubyPowder),        (getskilllv(SM_BASH)+1)*2,  getitemlink(RubyPowder));
            mesc l("@@/@@ @@", countitem(StrengthPotion),    (getskilllv(SM_BASH)+1)*2,  getitemlink(StrengthPotion));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(ManaPiouFeathers) < (getskilllv(SM_BASH)+1)*15 ||
                    countitem(CaveSnakeSkin)    < (getskilllv(SM_BASH)+1)*4 ||
                    countitem(RubyPowder)       < (getskilllv(SM_BASH)+1)*2 ||
                    countitem(StrengthPotion)   < (getskilllv(SM_BASH)+1)*2) goto L_Missing;

                delitem PiberriesInfusion, (getskilllv(SM_BASH)+1)*15;
                delitem CaveSnakeSkin,     (getskilllv(SM_BASH)+1)*4;
                delitem RubyPowder,        (getskilllv(SM_BASH)+1)*2;
                delitem StrengthPotion,    (getskilllv(SM_BASH)+1)*2;

                sk_lvup(SM_BASH);

                next;
            }
            break;
        case 2:
            mesc l("[Endure]");
            mesc l("Temporary immunity to move stun when hit, and MDEF bonus.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(PiberriesInfusion), (getskilllv(SM_ENDURE)+1)*10, getitemlink(PiberriesInfusion));
            mesc l("@@/@@ @@", countitem(DiamondPowder),     (getskilllv(SM_ENDURE)+1)*2,  getitemlink(DiamondPowder));
            mesc l("@@/@@ @@", countitem(MoubooSteak),       (getskilllv(SM_ENDURE)+1)*4,  getitemlink(MoubooSteak));
            mesc l("@@/@@ @@", countitem(IronIngot),         (getskilllv(SM_ENDURE)+1)*1,  getitemlink(IronIngot));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(PiberriesInfusion) < (getskilllv(SM_ENDURE)+1)*10 ||
                    countitem(DiamondPowder)     < (getskilllv(SM_ENDURE)+1)*2 ||
                    countitem(MoubooSteak)       < (getskilllv(SM_ENDURE)+1)*4 ||
                    countitem(IronIngot)         < (getskilllv(SM_ENDURE)+1)*1) goto L_Missing;

                delitem PiberriesInfusion, (getskilllv(SM_ENDURE)+1)*10;
                delitem DiamondPowder,     (getskilllv(SM_ENDURE)+1)*2;
                delitem MoubooSteak,       (getskilllv(SM_ENDURE)+1)*4;
                delitem IronIngot,         (getskilllv(SM_ENDURE)+1)*1;

                sk_lvup(SM_ENDURE);

                next;
            }
            break;
        case 3:
            mesc l("[Counter Defense]");
            mesc l("During a very small amount of time, you'll counter any attack with double critical, and won't take the damage.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(PinkAntenna),       (getskilllv(KN_AUTOCOUNTER)+1)*10,  getitemlink(ManaPiouFeathers));
            mesc l("@@/@@ @@", countitem(CaveSnakeSkin),     (getskilllv(KN_AUTOCOUNTER)+1)*4,  getitemlink(CaveSnakeSkin));
            mesc l("@@/@@ @@", countitem(RubyPowder),        (getskilllv(KN_AUTOCOUNTER)+1)*2,  getitemlink(RubyPowder));
            mesc l("@@/@@ @@", countitem(StrengthPotion),    (getskilllv(KN_AUTOCOUNTER)+1)*2,  getitemlink(StrengthPotion));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(PinkAntenna)      < (getskilllv(KN_AUTOCOUNTER)+1)*10 ||
                    countitem(CaveSnakeSkin)    < (getskilllv(KN_AUTOCOUNTER)+1)*4 ||
                    countitem(RubyPowder)       < (getskilllv(KN_AUTOCOUNTER)+1)*2 ||
                    countitem(StrengthPotion)   < (getskilllv(KN_AUTOCOUNTER)+1)*2) goto L_Missing;

                delitem PinkAntenna,       (getskilllv(KN_AUTOCOUNTER)+1)*10;
                delitem CaveSnakeSkin,     (getskilllv(KN_AUTOCOUNTER)+1)*4;
                delitem RubyPowder,        (getskilllv(KN_AUTOCOUNTER)+1)*2;
                delitem StrengthPotion,    (getskilllv(KN_AUTOCOUNTER)+1)*2;

                sk_lvup(KN_AUTOCOUNTER);

                next;
            }
            break;
        case 4:
            mesc l("[Two Hands Quicken]");
            mesc l("Triggers an attack speed buff when using two swords.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(Cheese),            (getskilllv(KN_TWOHANDQUICKEN)+1)*50, getitemlink(Cheese));
            mesc l("@@/@@ @@", countitem(Coral),             (getskilllv(KN_TWOHANDQUICKEN)+1)*30, getitemlink(Coral));
            mesc l("@@/@@ @@", countitem(PiberriesInfusion), (getskilllv(KN_TWOHANDQUICKEN)+1)*20, getitemlink(PiberriesInfusion));
            mesc l("@@/@@ @@", countitem(HastePotion),       (getskilllv(KN_TWOHANDQUICKEN)+1)*10, getitemlink(HastePotion));
            mesc l("@@/@@ @@", countitem(DiamondPowder),     (getskilllv(KN_TWOHANDQUICKEN)+1)*3,  getitemlink(DiamondPowder));
            mesc l("@@/@@ @@", countitem(ElixirOfLife),      (getskilllv(KN_TWOHANDQUICKEN)+1)*1,  getitemlink(ElixirOfLife));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(Cheese)               < (getskilllv(KN_TWOHANDQUICKEN)+1)*50 ||
                    countitem(Coral)                < (getskilllv(KN_TWOHANDQUICKEN)+1)*30 ||
                    countitem(PiberriesInfusion)    < (getskilllv(KN_TWOHANDQUICKEN)+1)*20 ||
                    countitem(HastePotion)          < (getskilllv(KN_TWOHANDQUICKEN)+1)*10 ||
                    countitem(DiamondPowder)        < (getskilllv(KN_TWOHANDQUICKEN)+1)*3 ||
                    countitem(ElixirOfLife)         < (getskilllv(KN_TWOHANDQUICKEN)+1)*1) goto L_Missing;

                delitem Cheese,             (getskilllv(KN_TWOHANDQUICKEN)+1)*50;
                delitem Coral,              (getskilllv(KN_TWOHANDQUICKEN)+1)*30;
                delitem PiberriesInfusion,  (getskilllv(KN_TWOHANDQUICKEN)+1)*20;
                delitem HastePotion,        (getskilllv(KN_TWOHANDQUICKEN)+1)*10;
                delitem DiamondPowder,      (getskilllv(KN_TWOHANDQUICKEN)+1)*3;
                delitem ElixirOfLife,       (getskilllv(KN_TWOHANDQUICKEN)+1)*1;

                sk_lvup(KN_TWOHANDQUICKEN);

                next;
            }
            break;
        case 5:
            // All skills related may include the basic class skills if they're related.
            mesc l("WARNING: If you leave the subclass, you'll lose all skills related to it!"), 1;
            mesc l("This cannot be undone. Are you sure?"), 1;
            mes "";
            if (askyesno() == ASK_YES) {
                mes "";
                if (validatepin()) {
                    skill KN_TWOHANDQUICKEN, 0, 0;
                    skill KN_AUTOCOUNTER, 0, 0;
                    skill SM_ENDURE, 0, 0;
                    skill SM_BASH, 2, 0;
                    MAGIC_SUBCLASS=MAGIC_SUBCLASS^CL_BERSERKER;
                    mesc l("You abandoned the BERSERKER class!"), 1;
                    close;
                } else {
                    mesc l("Failed to validate pin. Aborting.");
                    next;
                }
            } else {
                mes "";
                mesc l("Operation aborted. Phew!");
                next;
            }
            break;
        default:
            goto L_Close;
    }

    goto L_Member;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, WarlordHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GoldenWarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, ShortGladius);
    setunitdata(.@npcId, UDT_WEAPON, JeansChaps);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;
}

