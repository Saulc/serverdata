// TMW2 Script
// Author:
//    Jesusalva
// Description:
//    Hunt mouboos, Summer Quest

012-1,68,74,0	script	Luffyx	NPC_RAIJIN,{
    .@q=getq(SQuest_Summer);
    mesn;
    if (season() == SUMMER) {
        mesq l("Yay it's summer! I love June 21st, and the summer vacations!");
        if (.@q < 1)
            goto L_SummerQuest;
    } else {
        mesq l("Aww it's not summer. I love June 21st, and the summer vacations!");
    }

L_Main:
    if (GHQUEST)
        GHQ_Assign(ForestMushroom, "Hurnscald", "175.000 GP, 60 "+getitemlink(StrangeCoin));
    close;

L_SummerQuest:
    next;
    mesn;
    // TODO: Sunglass
    mesq l("Hey, do you know what is good on summer? @@!", getitemlink(LuffyxSummerShorts));
    next;
    mesn;
    mes l("What about you bring me:");
    mes l("@@/80 @@", countitem(CactusCocktail), getitemlink(CactusCocktail));
    mes l("@@/40 @@", countitem(AppleCocktail), getitemlink(AppleCocktail));
    mes l("@@/25 @@", countitem(CherryCocktail), getitemlink(CherryCocktail));
    mes l("@@/1 @@", countitem(JeansShorts), getitemlink(JeansShorts));
    next;
    select
        l("Not now, thanks"),
        l("To be honest, I have that with me!");

    mes "";
    if (@menu == 1)
        goto L_Main;
    if (@menu == 2) {
        if (
            countitem(CherryCocktail) < 25 ||
            countitem(CactusCocktail) < 80 ||
            countitem(AppleCocktail)  < 40
            ) goto L_Lying;

        inventoryplace LuffyxSummerShorts, 1;
        delitem CherryCocktail, 25;
        delitem AppleCocktail,  40;
        delitem CactusCocktail, 80;
        delitem JeansShorts, 1;
        getitem LuffyxSummerShorts, 1;
        getexp 6500, 120;
        setq SQuest_Summer, 1;
        mesn;
        mesq l("Yay yay! Many thanks! Here, take the reward as promised!");
        next;
        mesn;
        mesq l("We can do this again on next summer!");
        goto L_Main;
    }

    close;

L_Lying:
    mesn;
    mesq l("Please don't lie to me...");
    goto L_Main;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, CommunityShirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, JeansChaps);
    setunitdata(.@npcId, UDT_HEADBOTTOM, DeepBlackBoots);
    setunitdata(.@npcId, UDT_WEAPON, BugSlayer);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 13);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .distance=4;
    .sex=G_MALE;
	end;

}

