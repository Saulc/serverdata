// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Main Storyline
//      NPC_SAMANTHA

009-2,67,60,2	script	Book Keeper	NPC_FEMALE,{
    .@n = getq(General_Narrator);
    if (.@n == 4) mesn;
    if (.@n == 4) mesc l("Anyway, I think you should go to Hurnscald Townhall, and speak with the mayor.");
    if (.@n == 4) next;
    if (.@n >= 4) goto L_Complete;
    if (.@n == 3) goto L_Report;
    if (.@n == 2) goto L_Advance;
    mesn;
    mesq l("Hello. I am the book keeper, responsible for keeping Halinarzo History.");
    // TODO: Import data from BR-002-5 (Royal Library) and make available story about TMW-BR
    close;

L_Advance:
    .@nt = getq2(General_Narrator);
    // Initial Value is 0.
    // .@nt tracks future value
    if (!.@nt)
        goto L_Prologue;

    // If you wait 7 - instead of 3 - hours, you are abusing.
    // So now, you must wait an extra hour.
    if (.@nt > santime()+(60*60*7)) {
        setq2 General_Narrator, santime()+(60*60);
        .@nt=santime()+(60*60);
    }
    // Wait time is over!
    if (.@nt <= santime())
        goto L_Complete;
    mesn;
    mesq l("Please help us with the bandits on the Canyon, and meanwhile, I'll seek the records for you. Just @@ more.", FuzzyTime(.@nt));
    close;

L_Prologue:
    mesn strcharinfo(0);
    mesq l("Hi! Apparently, I came from here and moved at the age of 4, but I had amnesia and can't remember!");
    next;
    mesn strcharinfo(0);
    mesq l("It would be very kind of you, if you could seek who I am!");
    next;
    mesn;
    mesq l("What are you talking about? You are who you are right now. There's no other you.");
    mesc l("Unless, of course, if Saulc cloned you. But then we must blame Saulc!");
    next;
    mesn;
    mesq l("Ah... I see. You are a lost soul, without parents, lost on the world with only some basic stuff.");
    next;
    mesn;
    mesq l("Alright, I'll look in the archives. I'll have an answer for you in three hours. Meanwhile, why don't you suppress the bandits on the cliff?");
    setq2 General_Narrator, santime()+(60*60*3);
    close;

L_Complete:
    mesn;
    mesq l("So, I looked the records. There was nothing really useful.");
    mesq lg("Like, I know you are female.", "Like, I know you are male.");
    next;
    mesn;
    mesq l("But, I found something really interesting! Ah... I hope you like history?");
    next;
    mesn l("HISTORY BOOK");
    mesc l("In these dark times, countless souls moved away from Halinarzo. All hope seemed lost.");
    mesc l("But then, Andrei Sakar appeared. Hope was not lost. We could still defend it.");
    next;
    mesn l("HISTORY BOOK");
    mesc l("Many sacrifices were done, but it was lost.");
    mesc l("Even the mighty Andrei Sakar could only barely escape alive. The Monster King was too strong.");
    mesc l("All hope was lost. We failed to protect it. We lost everyone who challenged the Monster King. Killed without mercy.");
    next;
    mesn l("HISTORY BOOK");
    mesc l("The Monster King was so powerful! It is impossible to recover the artifact now, and everyone will die!");
    mesc l("A few families, also known as the Ancient Families of the Soul Menhir (for whatever reasons that may be), departed.");
    next;
    mesn l("HISTORY BOOK");
    mesc l("They promised to travel to the ##BWorld Edge##b to fetch a Mana Fragment. The wisest of them said it was the only chance.");
    mesc l("They were never again seen. None of them. We burnt all records about everyone here, and blamed the Monster King.");
    mesc l("They must never know we had it. Their sacrifices must be forgotten. For the sake of all.");
    compareandsetq General_Narrator, 2, 3;
    close;

L_Report:
    mesn;
    mesq l("Do you want to read the story again? Or should we get to the most obvious point?");
    next;
    select
        l("I want to know what you found out again"),
        l("I want to know why you shown me that.");
    if (@menu == 1)
        goto L_Complete;
    mesn;
    mesq l("We have three vanishing sort of people on the story.");
    next;
    mesn;
    mes l("1- The coward families, who ran away when outlook was bad.");
    mes l("2- The defenders of the artifact, who lost their lives and left people behind.");
    mes l("3- The ancient families.");
    next;
    mesn;
    mesq l("You probably was only 4 when all that happened. And opposed to official story, the Monster King never enter this building.");
    next;
    mesn;
    mesq l("In other words: If Lua couldn't find your record, your parents were part of this story! But, on which of these three groups were them?");
    next;
    mesn;
    mesq l("Well, actually, you could help me, traveling to the World's Edge! Ah, that is such dangerous journey though... You are not level 150 yet, are you?");
    next;
    mesn;
    mesq l("Anyway, I think if you go to Hurnscald Townhall, and speak with the mayor, he can help you getting there. You can't reach there normally, after all.");
    next;
    mesn;
    mesq l("Good luck, @@!", strcharinfo(0));
    getexp 1500, 0; // Reference is level 20, so, DO NOT RAISE THIS VALUE.
    setq General_Narrator, 4;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansShorts);
    setunitdata(.@npcId, UDT_HAIRSTYLE, any(8,11,20));
    setunitdata(.@npcId, UDT_HAIRCOLOR, 5);
    npcsit;

    .sex=G_FEMALE;
    .distance=5;
    end;
}

