// TMW2 scripts.
// Authors:
//    Saulc
// Variables:
//    CandorQuest_Vincent  - quest var


005-1,92,95,0	script	Vincent	NPC_PLAYER,{


    .LegsID = BugLeg;
    .LegsCount = 12;

    .@q = getq(CandorQuest_Vincent);
    if (.@q == 1) goto L_CheckItems;
    if (.@q == 2) goto L_QuestDone;

    speech S_LAST_BLANK_LINE,
        l("I am making a figurin with Bug legs."),
        lg("Be a friend and bring me @@ @@.", "Be a friend and bring me @@ @@.", .LegsCount, getitemlink(.LegsID));

    switch (select(l("Ok, I'll be back in no time."),
                   l("Sorry, I'm doing other things at the moment.")))
    {
        case 1:
            setq CandorQuest_Vincent, 1;
            speech S_FIRST_BLANK_LINE,
                l("Thank you. I'll wait here.");
            close;
        case 2:
            speech S_FIRST_BLANK_LINE,
                l("But I'm almost out of @@...", getitemlink(.LegsID));
            close;
    }

L_CheckItems:
    if (countitem(.LegsID) < .LegsCount)
    {
        speech
            l("Sorry, but you don't have what I need."),
            l("I need @@ @@.", .LegsCount, getitemlink(.LegsID));
        close;
    }

    speech
        l("That's exactly what I needed!"),
        l("I will be forever grateful!");

    delitem .LegsID, .LegsCount;
    getexp 15, 5;
    Zeny = Zeny + 1000;
    message strcharinfo(0), l("You receive @@ GP!", 1000);
    setq CandorQuest_Vincent, 2;
    close;

L_QuestDone:
    speech
        l("Hey, @@!", strcharinfo(0)),
        l("My figurin is so nice!");
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShorts);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 25);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex = G_MALE;
    .distance = 5;
    end;
}
