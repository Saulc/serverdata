// TMW2 Script
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Marius the Bard is a bard. Helps player with the world's background story.

003-1,88,135,0	script	Marius The Bard	NPC_ELVEN_MAN_TRADER,{

//hello;
    mesn;
    mesq l("Hello adventurer, what may this humble minstrel do for you today?");
    next;

    menu
        l("Who, or what are you?"),       L_Who,
        l("Where exactly am I?"),         L_Where,
        l("Can you sing me a song?"),     L_Music,
        l("Actually, nothing. Bye!"),     L_Bye;

L_Who:
    mes "";
    mesn;
    mesq l("I am a bard! I used to be an elf trader, but music got me for good.");
    next;
    mesq l("Now I travel by the world, composing songs about the things I see.");
    next;
    mesq l("I try to improve my skills every day. One day, I will sing a song no one will ever forget.");
    close();

L_Where:
    mes "";
    mesn;
    mesq l("This is Tulishmar, the oldest of human cities.");
    next;
    mesq l("It is a port city at the south of the continent. Main economic activities are mining and spices trade.");
    next;
    mesq l("The biggest mine, where you could find Mana Stones in the past, is now entirely depleted. Not a single stone left.");
    next;
    mesq l("The magic council have only a handful set of them, and they let only the most skilled ones get it.");
    next;
    mesq l("If you want, you can ask there for one. They are at the big building at northwest.");
    close();

// NOTE: Rewriting this song is always OK.
// The last line is always bigger than the previous ones.
L_Music:
    mes "";
    mesn;
    mesq l("I will sing a song about the Mana War and current times.");
    next;
    mes l("In times of famine, magic came to save us.");
    mes l("But we took every stone, and restricted the magic.");
    mes l("Ah! Greedy humans! Why had we to desire?");
    mes l("Ah! Greedy humans! How ignorant were we, to ever do that?");
    next;
    mes l("Saul and Fefe did a great revolution,");
    mes l("They wanted magic to be once again free!");
    mes l("Ah! Greedy humans! Why did we had to fight?");
    mes l("Ah! Greedy humans! Couldn't we happy with little?");
    next;
    mes l("The mana war stroke, and many people died,");
    mes l("The Mana Stones were stolen, in evil hands have fallen!");
    mes l("Ah! Greedy humans! Just how big is our greed?");
    mes l("Ah! Greedy humans! Things will never be as they used to be!");
    next;
    mes l("The mage thief tried to all power absorb,");
    mes l("But instead of dying a Monster King he became!");
    mes l("Ah! Greedy humans! We pay the price for our actions,");
    mes l("Ah! Greedy humans! The Monster War will now rage the globe!");
    next;
    mes l("But hope is not lost, said the Sages of Fate!");
    mes l("Because heroes are not born, rather, they are made!");
    mes l("Ah! Greedy humans! Stand up to save our world!");
    mes l("Ah! Greedy humans! End this war which our greed has stroke!");
    close();

L_Bye:
    mes "";
    mesn;
    mesq lg("Good bye my friend, and safe travels!");
    close();

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
