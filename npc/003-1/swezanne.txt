// TMW-2 Script
// Author:
//    Jesusalva
// Note:
//    DO NOT USE QUEST STATUS 2. Thing from past.

003-1,70,100,0	script	Swezanne	NPC_FEMALE,{
    .@q=getq(TulimsharQuest_Swezanne);
    mesn;
    mesq l("Hi.");
    next;
    .@q2=getq2(TulimsharQuest_Swezanne);
    if (.@q == 0) goto L_Quest;
    else if (.@q == 3) goto L_Silvia;
    else if (.@q2 < santime()) goto L_Repeat;
    mesq l("I fight every day in hopes to meet Andrei, the famous hero who prevented Hurnscald from total destruction against a horde of monsters alone.");
    if ($MOST_HEROIC$ != "") goto L_Heroics;
    close;

L_Heroics:
    next;
    mesn;
    .@d=rand(1,6);

    if      (.@d == 1) .@deed$="protected our cities!";
    else if (.@d == 2) .@deed$="did great acts of bravery!";
    else if (.@d == 3) .@deed$="is just awesome!";
    else if (.@d == 4) .@deed$="killed a monster army single-handed!";
    else if (.@d == 5) .@deed$="proved their worth in battlefield!";
    else if (.@d == 6) .@deed$="impressed even the High Council!";
    else .@deed$="is awesome like me!"; // Should not happen

    mesq l("I hope to one day be like @@, who @@", $MOST_HEROIC$, .@deed$);
    close;

L_Quest:
    mesq l("Ah, fighting monsters under this desert heat makes me thirsty. But someone must do this job, otherwise Tulimshar could fall.");
    next;
    mesq l("Maybe you could bring me 5 delicious @@? They have a great effect in quenching thirst and recovering vigour.", getitemlink(CactusDrink));
    menu
        rif(countitem(CactusDrink) >= 5, l("Here they are!")), L_Finish,
        l("I'll get to it."), L_Close;
    close; // double sure

L_Repeat:
    mesq l("Ah, fighting monsters under this desert heat makes me thirsty. But someone must do this job, otherwise Tulimshar could fall.");
    next;
    mesq l("Maybe you could bring me 5 delicious @@? They have a great effect in quenching thirst and recovering vigour.", getitemlink(CactusPotion));
    menu
        rif(countitem(CactusPotion) >= 5, l("Here they are!")), L_Finish2,
        l("I'll get to it."), L_Close;
    close;

L_Finish2:
    delitem CactusPotion, 5;
    getexp 37, 0;
    Zeny = (Zeny + 220); // 5*35 = 175 base
    setq TulimsharQuest_Swezanne, 1;
    setq2 TulimsharQuest_Swezanne, gettimetick(2)+60*60*24;
    close;

L_Finish:
    delitem CactusDrink, 5;
    getexp 63, 4; // 4 Job points! A great reward.
    Zeny = (Zeny + 150); // 5*25 = 125 base
    setq TulimsharQuest_Swezanne, 3;
    setq2 TulimsharQuest_Swezanne, gettimetick(2)+60*60*24;
    close;

L_Silvia:
    mesn;
    mesq l("Ah, my daughter Silvia is so far away... But I don't want to leave the shade of this tree...");
    next;
    mesn;
    mesq l("Perhaps you could tell her how much I love her? It is already some days since I last talked to her.");
    next;
    mesn;
    mesq l("I don't want her to think that I am a bad mother.");
    mes "";
    menu
        l("Sure, I'll do it."), L_SilviaAccept,
        l("One day more or less won't make a difference."), L_Close;

L_SilviaAccept:
    mes "";
    mesn;
    mesq l("Oh, thank you! My daughter means a lot to me.");
    setq TulimsharQuest_Swezanne, 4;
    close;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, BromenalChest);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CottonTrousers);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .sex = G_FEMALE;
    .distance = 5;


    /*
    // Preventive check against faulty update. Must be removed afterwards.
    // UPDATE `quest` SET `count1` = '3' WHERE `quest`.`quest_id` = 54 AND `count1` != '1';
	.@nb = query_sql("select `char_id` from `quest` WHERE ((`count1`=1 or `count1`=2) and `quest_id`=54) LIMIT 2", .@name$);
    if (getarraysize(.@name$) > 0) {
        debugmes "FATAL ERROR: Quest log not updated.";
        debugmes "disabling Swezanne to prevent weirder bugs.";
        disablenpc .name$;
    }
    */
    end;
}
