// TMW2 scripts.
// Author:
//    Saulc

003-1,108,110,0	trader	Shop#bazar1	NPC_NO_SPRITE,{

OnInit:
    tradertype(NST_MARKET);

    sellitem YellowDye, -1, 2;
    sellitem Knife, -1, 5;
    sellitem Arrow, -1, 30000;
    sellitem DesertHat, -1, 8;
    sellitem SilkRobe, -1, 1;
    sellitem Bread, -1, 15;
    sellitem CroconutBox, rand(2800,3050), 6;
    sellitem EmptyBottle, -1, 3; // You can buy some empty bottles here, but they're scarse

    .sex = G_OTHER;
    .distance = 3;
    end;

OnClock0621:
OnClock1210:
OnClock1757:
OnClock0000:
    restoreshopitem YellowDye, -1, 2;
    restoreshopitem Knife, -1, 5;
    restoreshopitem Arrow, -1, rand(10000,30000);
    restoreshopitem DesertHat, -1, 8;
    restoreshopitem SilkRobe, -1, 1;
    restoreshopitem Bread, -1, 15;
    restoreshopitem CroconutBox, rand(2800,3050), 6;
    restoreshopitem EmptyBottle, -1, 3;
}

