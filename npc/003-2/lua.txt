// TMW2 Scripts.
// Author:
//  Jesusalva
// Description:
//  Lua is an Alliance Officer. She takes care of miscelanneous administrative tasks, including Strange Coin generation.
//  She can also change the world's hero, give Sponsor their necklace, and is part of main storyline.

003-2,38,34,0	script	Lua#003-2	NPC_FEMALE,{
    .@n = getq(General_Narrator);

    mesn;
    mesq l("Hello, I act on the Alliance's behalf.");

L_Menu:
    mes "";
    menu
        l("Ok, see you later."),L_Close,
        rif(.@n == 1, l("I was found near Candor Island, on a raft, and can't remember anything. Do you know who I am?")), L_Info,
        rif(is_gm(), l("I need a GM set, please!")), L_GMItems,
        rif(is_gm() && #GMEVENT_T <= gettimetick(2), l("I plan in doing an event! Give me the coins!")), L_GMEvent,
        rif(is_staff(), l("Tell people the name of a new hero.")), L_NewHero,
        rif((getgmlevel() == 1 || is_admin()) && #T_SPONSOR <= gettimetick(2), l("I want a Sponsor Necklace.")), L_Sponsor,
        l("I am a contributor, and I want contributor stuff."), L_Contributor,
        l("What alliance?"),-;
    mes "";
    mesn;
    mesq l("The Alliance which was formed after the war!");
    close;

L_GMItems:
    getitembound "GMRobe", 1, 1;
    getitembound "GMCap", 1, 1;
    logmes(strcharinfo(0)+" just took a GM set.");
    logmes(strcharinfo(0)+" just took a GM set.", LOGMES_ATCOMMAND);
    mes "";
    mesn;
    mesq lg("Here they are, miss.", "Here they are, mister.");
    mes "";
    goto L_Menu;

L_GMEvent:
    #GMEVENT_T=gettimetick(2)+(60*60*24);
    getitem "StrangeCoin", 20;
    logmes(strcharinfo(0)+" just took twenty event coins.");
    logmes(strcharinfo(0)+" just took twenty event coins.", LOGMES_ATCOMMAND);
    mes "";
    mesn;
    mesq lg("Here they are, miss.", "Here they are, mister.");
    mes "";
    goto L_Menu;

L_NewHero:
    mes "";
    mes l("Current hero: @@", $MOST_HEROIC$);
    input .@MH$;
    logmes "Changing hero from "+$MOST_HEROIC$+" to "+.@MH$;
    logmes(strcharinfo(0)+" changed the world's hero.", LOGMES_ATCOMMAND);
    $MOST_HEROIC$ = .@MH$;
    goto L_Menu;

L_Sponsor:
    #T_SPONSOR=gettimetick(2)+(60*60*24*30);
    rentitem "SponsorNecklace", (60*60*24*30);
    logmes(strcharinfo(0)+" just took a Sponsor Necklace.");
    logmes(strcharinfo(0)+" just took a Sponsor Necklace.", LOGMES_ATCOMMAND);
    mes "";
    mesn;
    mesq lg("Here it is, miss.", "Here it is, mister.");
    mes col("This item will be returned in 30 days counting from now, and you will need to take another.", 9);
    mes col("It cannot be dropped, traded, sold, but you can store it and use with same account.", 9);
    mes "";
    goto L_Menu;

L_Info:
    mes "";
    mesn;
    mesq l("Of course, give me just a minute to look up for your data, @@.", strcharinfo(0));
    next;
    mes "...";
    next;
    mes "... ...";
    next;
    mes "... ... ...";
    next;
    mesn;
    mesq l("No criminal record, no bank account, not even a tax payment declaration.");
    next;
    mesq l("I found your birth record, but it is not complete. Here says you were born on Halinarzo.");
    next;
    mesq l("The problem is that Halinarzo was destroyed in the never-ending war and was just recently rebuilt.");
    next;
    mesq l("Almost all documents were lost, destroyed or damaged. I cannot even find your parents name.");
    next;
    mesq l("All I can say is that you were born there, and moved by the age of 4, but to where? I don't know.");
    next;
    mesn;
    mesq l("I'll arrange here with the alliance documents regarding you. I'll add a \"war victim\" status to make your life easier.");
    next;
    mesq l("You will then be able to register citzenship in any city, for free. But just once.");
    next;
    mesn;
    mesq l("Sorry. At best, you can try your luck in Halinarzo, but the odds of someone recognizing you are pretty slim.");
    next;
    mesq l("You will be killed on the road if you try to go like this. Try helping people. Get money, experience, equipment.");
    next;
    mesq l("And then, finally, depart to ##BHalinarzo##b. Maybe, just maybe, someone recognize you. Or, start a new life! Few people can do that!");
    setq General_Narrator, 2;
    close;

L_Contributor:
    mes "";
    .@m = htget($@CONTRIBUTORS, strtolower(strcharinfo(0)), 0);
    if (!.@m) {
        mesn;
        mesq l("Ah... Sorry, your name is not on the contributor list.");
        next;
        mesn;
        mesq l("You can colaborate with our project though! Just ask on #tmw2-dev, be it on Discord or IRC!");
        close;
    }
    mesn;
    mesq l("Ah, welcome @@. You have @@ Contributor Points.", strcharinfo(0), .@m);
    mesq l("Let's see if you can pick a reward!");
    select
        rif(!(#CRW & 32) && .@m >= 10000 && Zeny > 100000, l("(100,000 GP) Legendary Mount")),
        rif(!(#CRW & 16) && .@m >= 4500, l("Delicious Cookie")),
        rif(!(#CRW & 8) && .@m >= 2000, l("Developer Cap")),
        rif(!(#CRW & 4) && .@m >= 500, l("Contributor Sweater")),
        rif(!(#CRW & 2) && .@m >= 100, l("Community Shirt")),
        rif(!(#CRW & 1) && .@m, l("5x Strange Coins")),
        l("Which rewards are available?"),
        l("Ok, thanks.");

    mes "";
    switch (@menu) {
        case 1:
            Zeny=Zeny-100000; getitembound "LegendaryMount", 1, 1; #CRW=#CRW|32 ; break;
        case 2:
            getitem "DeliciousCookie", 1; #CRW=#CRW|16 ; break;
        case 3:
            getitem "DEVCap", 1; #CRW=#CRW|8 ; break;
        case 4:
            getitem "ContributorSweater", 1; #CRW=#CRW|4 ; break;
        case 5:
            getitem "CommunityShirt", 1; #CRW=#CRW|2 ; break;
        case 6:
            getitem "StrangeCoin", 5; #CRW=#CRW|1 ; break;
        case 7:
            mes l("10000 (+100,000 GP) - @@", getitemlink(LegendaryTortuga));
            mes l("4500 - @@", getitemlink(DeliciousCookie));
            mes l("2000 - @@", getitemlink(DEVCap));
            mes l("500 - @@", getitemlink(ContributorSweater));
            mes l("100 - @@", getitemlink(CommunityShirt));
            next;
            goto L_Contributor;
            break;
    }
    close;

L_Close:
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GMRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, LousyMoccasins);
    setunitdata(.@npcId, UDT_WEAPON, BugSlayer);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 5);

    .sex = G_FEMALE;
    .distance = 4;
    end;
}
