// TMW2 Scripts.
// Author:
//    Jesusalva
// Description:
//    Nard's ship Doors NPCs.

002-3,30,28,0	script	AreaNPC#doors4	NPC_HIDDEN,0,0,{

OnTouch:
    if (LOCATION$ == "Candor") {
        warp "005-1", 42, 107;
        close;
    }
    if (LOCATION$ == "Tulim") {
        warp "003-1", 81, 68;
        close;
    }

    setcamnpc "Julia";
    mesn "Julia";
    mesq l("The captain has locked the door, you should go see him.");
    next;

    mesn "Narrator";
    mesc l("Captain Nard is in the room to your right.");
    next;
    restorecam;

    close;


OnInterIfInit:

    // Candor Instance (we do all tests for debugmes here)
    .CANDOR_INSTID = instance_create("002-1@CandorInst", 0, IOT_NONE);
    if (.CANDOR_INSTID < 0) debugmes "Error: No instance ID";
    else {
        debugmes "created new instance id: " + str(.CANDOR_INSTID);
        .CANDOR1$=instance_attachmap("002-1", .CANDOR_INSTID, 0, "002-1@Candor");
        if (.CANDOR1$ == "") debugmes "Error: Map 002-1 CANDY failed";

        .CANDOR3$=instance_attachmap("002-3", .CANDOR_INSTID, 0, "002-3@Candor");
        if (.CANDOR3$ == "") debugmes "Error: Map 002-3 CANDY failed";

        .CANDOR4$=instance_attachmap("002-4", .CANDOR_INSTID, 0, "002-4@Candor");
        if (.CANDOR4$ == "") debugmes "Error: Map 002-4 CANDY failed";

        debugmes "Nard's Ship in Candor instance is set.";
        instance_set_timeout(1000000, 1000000, .CANDOR_INSTID);
        instance_init(.CANDOR_INSTID);
    }

    // Tulim Instance
    .TULIM_INSTID = instance_create("002-1@TulimInst", 0, IOT_NONE);
    instance_attachmap("002-1", .TULIM_INSTID, 0, "002-1@Tulim");
    instance_attachmap("002-3", .TULIM_INSTID, 0, "002-3@Tulim");
    instance_attachmap("002-4", .TULIM_INSTID, 0, "002-4@Tulim");
    instance_set_timeout(1000000, 1000000, .TULIM_INSTID);
    instance_init(.TULIM_INSTID);
    end;
}

002-3,44,28,0	script	AreaNPC#doors5	NPC_HIDDEN,0,0,{

OnTouch:
    .@julia = getq(ShipQuests_Julia);
    if (.@julia >= 2) goto L_Warp;
    close;

L_Warp:
    if (LOCATION$ == "") 
        warp "002-4", 20, 27;
    else
        warp "002-4@"+LOCATION$, 20, 27;
    close;
}

002-3,42,25,0	script	AreaNPC#002-3d	NPC_HIDDEN,0,0,{

OnTouch:
    if (LOCATION$ == "") 
        warp "002-1", 72, 29;
    else
        warp "002-1@"+LOCATION$, 72, 29;
    close;
}
