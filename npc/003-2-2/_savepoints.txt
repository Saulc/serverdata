// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 003-2-2: Second Deck saves
003-2-2,40,37,0	script	#save_003-2-2_40_37	NPC_SAVE_POINT,{
    savepointparticle .map$, .x, .y, CURRENT_INN;
    close;

OnInit:
    .distance = 2;
    .sex = G_OTHER;
    end;
}
003-2-2,46,37,0	script	#save_003-2-2_46_37	NPC_SAVE_POINT,{
    savepointparticle .map$, .x, .y, CURRENT_INN;
    close;

OnInit:
    .distance = 2;
    .sex = G_OTHER;
    end;
}
003-2-2,55,40,0	script	#save_003-2-2_55_40	NPC_SAVE_POINT,{
    savepointparticle .map$, .x, .y, CURRENT_INN;
    close;

OnInit:
    .distance = 2;
    .sex = G_OTHER;
    end;
}
003-2-2,50,38,0	script	#save_003-2-2_50_38	NPC_SAVE_POINT,{
    savepointparticle .map$, .x, .y, CURRENT_INN;
    close;

OnInit:
    .distance = 2;
    .sex = G_OTHER;
    end;
}
003-2-2,53,38,0	script	#save_003-2-2_53_38	NPC_SAVE_POINT,{
    savepointparticle .map$, .x, .y, CURRENT_INN;
    close;

OnInit:
    .distance = 2;
    .sex = G_OTHER;
    end;
}
