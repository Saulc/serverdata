// TMW2 Script
// Author:
//      Saulc
// Reviewer:
//    Jesusalva
// Description:
//    craft desert shirt. Reference to player msawis
// id:193  HalinarzoQuest_Sawis

010-2,138,122,0	script	Sawis	NPC_PLAYER,{
    .@q = getq(HalinarzoQuest_Sawis);
    if (BaseLevel < 37) goto L_TooWeak;
    if (.@q == 1) goto L_Check;
    if (.@q == 2) goto L_Complete;

L_GiveTask:
    mesn;
    mesq lg("Hello, wanderer!");
    next;
    mesq l("No one visit me, I'mm in peace there.");
    next;

    menu
        l("What are you doing alone in this desert?"), L_Quest,
        l("Don't speak to me crazy guy!"), L_Quit;

L_Quest:
    mes "";
    mesn;
    mesq l("I do some research about evolution of snake skin subject to hard sunlight.");
    next;
    mesn;
    mesq l("Then I craft some new type of strong desert shirt, to protect users from snakes and sunlight.");
    next;
    mesn;
    mesq l("If you bring me some items, I can craft one for you. You will be my test subject.");
    next;

    menu
        l("Yeah, sure? What do you need?"), L_Start,
        l("Nah, sorry, everything is good with me."), L_Quit;

L_Start:
    setq HalinarzoQuest_Sawis, 1;
    mes "";
    mesn;
    mesq l("Ok, what I need is:");
    goto L_List;

L_Quit:
    mes "";
    mesn;
    mesq l("Alright.");
    close;

L_List:
    mes "";
    mesn;
    mes l("Here's what I need:");
    mes l("@@/1 @@", countitem(LeatherShirt), getitemlink(LeatherShirt));
    mes l("@@/1 @@", countitem(CottonCloth), getitemlink(CottonCloth));
    mes l("@@/1 @@", countitem(CaveSnakeSkin), getitemlink(CaveSnakeSkin));
    mes l("@@/3 @@", countitem(SnakeSkin), getitemlink(SnakeSkin));
    close;

L_Check:
    mesn;
    mesq l("Did you brought me everything I asked for?");
    next;
    menu
        l("Yes!"), L_Give,
        l("Sorry, I forgot what you need!"), L_List,
        l("No!"), L_Quit;

L_Give:
    if (
        countitem(LeatherShirt)         < 1  ||
        countitem(CottonCloth)          < 1  ||
        countitem(CaveSnakeSkin)        < 1  ||
        countitem(SnakeSkin)            < 3
        ) goto L_Lying;

    inventoryplace DesertShirt, 1;

    delitem(LeatherShirt, 1);
    delitem(CottonCloth, 1);
    delitem(CaveSnakeSkin, 1);
    delitem(SnakeSkin, 3);

    getitem(DesertShirt, 1);
    getexp(5000, 30);
    setq(HalinarzoQuest_Sawis, 2);

    mes "";
    mesn;
    mesq l("I hope this is your size.");
    close;

L_Complete:
    mesn;
    mesq l("As I see, you stay alive!");
    next;
    mesn;
    mesq l("I must keep notes of this.");
    close;

// Funnier to write than to read, but the player lied. :angel:
L_Lying:
    mesn;
    mesq l("No no no, that's wrong.");
    next;
    mesc l("Actually, you could be really useful testing Snake Poison. What do you think about that?");
    next;
    goto L_List;

L_TooWeak:
    mesn;
    mesq l("What are you doing here? This place is too dangerous for you!");
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADMIDDLE, DesertShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .sex = G_MALE;
    .distance = 5;

    end;
}
