// TMW2 Scripts
// Author: Crazyfefe
//         Jesusalva
// Desc:   Mob Points for Aidan & Ishi. You will gain MONSTER-LEVEL mob points.

function	script	mobpoint	{
    if (!MPQUEST)
        return;
    //if (killedrid < 1002) goto L_Return;

    // You get MobLv + 10% as MobPoints.
    // So a level 100 monster gives you 110 MobPt.
    .@addval=strmobinfo(3,killedrid)*11/10;
    Mobpt = Mobpt + .@addval;
    return;

}

000-0,0,0,0	script	#mobptsys	NPC_HIDDEN,{
    end;

OnNPCKillEvent:
    if (killedrid == MonsterKing) {
        announce "An illusionary monster king was killed.", bc_all | bc_pc;
        getexp BaseLevel**3, 0;
    }

    // Remove undue Job exp
    // The check is probably correct, but setparam is not working =/
    /*
    if (strmobinfo(7, killedrid) == 0 && readparam(JobExp) > 0) {
        setparam(JobExp, readparam(JobExp)-1);
    }
    */

    // call functions
    callfunc "mobpoint";
    callfunc "SQuest_Hasan";
    end;

}
