// TMW2/LoF Script
// Author:
//  Jesusalva
// Description:
//  Lockpicking core

// Important variables:
//  THIEF_EXP
//      Experience on Thief Tree
//  THIEF_RANK
//      Position on the Thief Tree

// LockPicking(num_pins, max_pins)
// Returns 0 upon failure, 1 upon success
// Closes script if an error happen or if you give up / cannot try.
//
// The 'next' is upon script responsability
// Maximum pin number is infinite. Maximum Pin Positiors range from 2~5.
// If you fail, you can end up having to start again. If you fail too much,
// you'll be caught!
function	script	LockPicking	{
    // If you don't have a LockPick, you can't do this (useless)
    if (!countitem(Lockpicks)) {
        mesc l("You need a @@ to try this.", getitemlink(Lockpicks)), 1;
        close;
    }

    .@d=getarg(0,1);
    .@m=getarg(1,3);

    // Invalid Argument (kill script)
    if (.@d < 1 || .@m < 2 || .@m > 5)
        end;

    // You must be rank (number of locks - 1) to try
    if (THIEF_RANK+1 < .@d) {
        mesc l("This lock is beyond your current capacity."), 1;
        close;
    }

    // Create @pins array (the answer)
    for (.@i=0; .@i < .@d;.@i++)
        @pins[.@i] = rand(1,.@m);

    // Check if you'll try to open it.
    mesc l("This lock is simple, maybe with your thief skills you can manage to pry it open. But beware, you can end up in jail!");
    mesc l("Will you try to unlock it?");
    if (askyesno() == ASK_NO)
        close;

    // Setup your attempt
    delitem Lockpicks, 1;
    @pos=0;
    mesc l("You insert the hook pick inside the lock, and, without applying any tension, you discover there are only @@ pins to set.", .@d);

    // You have as many attempts as pins and appliable strenghts.
    // Each thief rank grants you an extra attempt.
    // Each pin takes one attempt.
    // It's not multiplied, so 3 pins with 3 positions: 6 chances, 9 possibilities.
    // There's no penalty, but the attempt is counted working or not!
    // Remember if you fail, all previous pins will be cleared (@pos)
    for (.@i=0; .@i < (.@d+.@m+THIEF_RANK) ; .@i++) {
        mesc l("You are trying to open the @@th pin. What will to do?", @pos+1);

        menuint
            rif(.@m >= 4, l("Apply no pressure")), 4,
            rif(.@m >= 2, l("Apply soft pressure")), 2,
            rif(.@m >= 1, l("Apply normal pressure")), 1,
            rif(.@m >= 3, l("Apply strong pressure")), 3,
            rif(.@m >= 5, l("Apply very strong pressure")), 5,
            l("Give up!"), 0;

        if (!@menuret)
            close;

        // Is your guess correct?
        if (@pins[@pos] == @menuret) {
            mesc l("*click*");
            @pos+=1;
        } else {
            mesc l("This didn't work. All pins are now unset!");
            @pos=0;
            // We don't need to clear console, each successful attempt IS counted.
            // Therefore, unsetting 3 pins means you must do 3 new attempts!!
            // The biggie is that you're running against time, here!!!
        }

        if (@pos >= .@d) {
            THIEF_EXP += .@d*.@m-THIEF_RANK;
            return 1;
        }
    }

    THIEF_EXP += 1;
    return 0;
}
