// TMW2 Scripts
// Author:
//   Jesusalva
// Description:
//   TODO


001-6,159,157,0	script	Soren Village	NPC_SUMMONING_CIRC,0,0,{
    end;

OnTouch:
    if (@SorenWarpOnline == 1)
        end;
    dispbottom l("Loading warp, be at this spot in 15 seconds.");
    @SorenWarpOnline=1;
    addtimer 15000, "Soren Village::OnWarper";
    end;

OnWarper:
    @SorenWarpOnline=0;
    if (isin("001-6", 159, 157, 159, 157) && !ispcdead()) {
        compareandsetq HurnscaldQuest_Celestia, 2, 3;
        warp "soren", 179, 76;
    } else {
        dispbottom l("Failed to warp to Soren Village.");
    }
    end;

OnInit:
    .sex = G_OTHER;
    end;
}
